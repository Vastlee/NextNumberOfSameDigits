﻿using UnityEngine;
using UnityEngine.UI;

public class DigitManager : MonoBehaviour {
    [SerializeField]
    GameObject digitPrefab;
    [SerializeField]
    Transform digitsContainer;
    [SerializeField]
    float digitSpacer = 1.0f;

    GameObject[] digits;
    int[] digitInts;

    #region MonoBehaviour
    void Start() {
        CreateDigits("123456789012345678901234");
    }
    #endregion

    #region Methods
    private void CreateDigits(string num) {
        this.digitInts = StringToIntArray(num);
        this.digits = new GameObject[this.digitInts.Length];
        float digitDistance = this.digitPrefab.GetComponent<RectTransform>().rect.width;
        digitDistance += this.digitSpacer;
        Vector3 nextPos = this.digitPrefab.transform.localPosition;

        for(int i = 0; i < this.digitInts.Length; i++) {
            digits[i] = Instantiate(this.digitPrefab, this.digitsContainer);
            digits[i].GetComponentInChildren<Text>().text = this.digitInts[i].ToString();
            digits[i].transform.localPosition = nextPos;
            digits[i].SetActive(true);
            nextPos.x -= digitDistance;
        }
    }

    private long NextBiggerNumber(long n) {
        long result = -1;
        int[] nums = StringToIntArray(n.ToString());
        if(!IsLongEnough(nums)) { return -1; }
        int? foundIndex = IndexOfLastAscendingPair(nums);
        if(foundIndex.HasValue) {
            Swap(ref nums[foundIndex.Value], ref nums[IndexOfSmallestFromFound(nums, foundIndex.Value)]);
            System.Array.Sort(nums, (foundIndex.Value + 1), (nums.Length - 1) - foundIndex.Value);
            result = IntArrayToLong(nums);
        }
        return result;
    }

    private int? IndexOfLastAscendingPair(int[] nums) {
        int? result = null;
        for(int i = (nums.Length - 1); i > 0; i--) {
            if(nums[i] > nums[i - 1]) {
                result = i - 1;
                break;
            }
        }
        return result;
    }

    private int IndexOfSmallestFromFound(int[] nums, int foundIndex) {
        int result = foundIndex + 1;
        for(int i = nums.Length - 1; i >= foundIndex + 1; i--) {
            if(nums[i] > nums[foundIndex] && nums[i] < nums[result]) {
                result = i;
            }
        }
        return result;
    }

    void Swap(ref int firstIndex, ref int secondIndex) {
        int tmp = firstIndex;
        firstIndex = secondIndex;
        secondIndex = tmp;
    }

    long IntArrayToLong(int[] nums) {
        long result = 0;
        long multiplier = 1;
        for(int i = nums.Length - 1; i >= 0; i--) {
            result += nums[i] * multiplier;
            multiplier *= 10;
        }
        return result;
    }

    int[] StringToIntArray(string str) {
        int[] result = new int[str.Length];

        for(int i = 0; i < str.Length; i++) {
            result[i] = (int) System.Char.GetNumericValue(str[i]);
        }
        return result;
    }

    private bool IsLongEnough(int[] nums) {
        return (nums.Length > 1);
    }
    #endregion
}
